package no.uib.inf101.terminal;

class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        String s = "";
        for (String item : args) {
            s += item + " ";
        }
        return s;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
